import groovy.json.*
import java.util.zip.*

def url = 'https://api.stackexchange.com/2.2/tags?order=desc&max=9999&sort=popular&site=stackoverflow'.toURL()

url.withInputStream {is ->
    def input = new GZIPInputStream(is)
    def json  = new JsonSlurper().parseText(input.text)
    //println json.items.collect{"${it.name}:${it.count}"}
    def items = json.items.collect { [it.name, it.count] }
    int W     = items*.get(0)*.size().max()
    items.each { println "${it[0].padRight(W)}: ${it[1]}" }
}
''
