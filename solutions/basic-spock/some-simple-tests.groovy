@Grab('org.spockframework:spock-core:1.1-groovy-2.4')
class tst extends spock.lang.Specification {

    def 'upper case text'() {
        given: 'a text string'
        def s = 'tjolla hopp'
        
        when: 'make it into uppercase'
        s = s.toUpperCase()
        
        then: 'it should work'
        s == 'TJOLLA HOPP'
    }
    
    def 'reversing a text'() {
        given: 'a text string'
        def s = 'abcd'
        
        when: 'reversing it'
        s = s.reverse()
        
        then: 'the chars should be in reverse order'
        s == 'dcba'
    }
    
    def 'using a list'() {
        given: 'a list'
        def lst = [1,2,3]
        def size = lst.size()
        
        when: 'appending a new element'
        lst = lst << 4
        
        then: 'its size should be +1'
        lst.size() == size+1
    }
    
    def 'using a stack'() {
        given: 'an empty stack'
        def stk = new Stack()
        
        when: 'pushing two elements'
        stk.push(10); stk.push(20)
        
        then: 'its size should be 2'
        stk.size() == 2
        
        when: 'popping of one element'
        stk.pop()
        
        then: 'its size should be 1'
        stk.size() == 1
    }
    
    def 'gauss integer sum formula'() {
        given: 'a positive number'
        int N = 10000
        
        when: 'using the formula'
        def result = N * (N+1) / 2
        
        then: 'it should be the sum of the numbers'
        result == (1..N).sum()
    }

}
