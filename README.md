Spock Unit Tests
====

Welcome to this course.
The syllabus can be find at
[groovy/spock](https://www.ribomation.se/groovy/spock.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to the sample project

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/groovy/spock.git
    cd spock

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have
the following installed. 

* [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Groovy](http://groovy-lang.org/download.html)
* [Gradle](https://gradle.org/install/)
* [Maven](https://maven.apache.org/download.cgi)
* [Grails](https://grails.org/download.html)

If you are running a BASH shell, then go for [SDKMAN](http://sdkman.io/index.html), 
which can download and install all tools above.

For decent HTTP REST WS client, install

* [HTTPie](https://httpie.org/)

In addition, you need to be using a decent Java IDE, such as any of

* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) - Our choice of IDE
* [MS Visual Code](https://code.visualstudio.com/download)
* [Eclipse](http://www.eclipse.org/downloads/eclipse-packages/)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

